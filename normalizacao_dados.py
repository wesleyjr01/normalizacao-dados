import pandas as pd
import time
start = time.time()

# Load Data
df_name = str(input('Insira o nome do arquivo a ser lido, em formato CSV.):\n'))
# df = pd.read_csv(df_name + '.csv', encoding = "ISO-8859-1", skiprows=2, sep=';')


def normaliza_dados(df_name):
  start = time.time()
  df = pd.read_csv(df_name + '.csv', encoding = "ISO-8859-1", sep=';')
  pd.set_option('display.max_columns', 50)

  ### Normaliza colunas
  print(f'\nNormalizando Cabecalho...')
  colunas = [f.
             lower().
             replace('ã', 'a').
             replace('º', '').
             replace('ó', 'o').
             replace('é', 'e').
             replace('á', 'a').
             replace('ç', 'c').
             replace(' ', '_').
             replace('.', '').
             replace('-', ' ').
             replace(':', '_').
  		       replace('ú', 'u').
             upper() for f in df.columns]
  df.columns = colunas

  ### Formatacao de Linhas
  print(f'\nNormalizando Linhas...')
  for column in df.select_dtypes(include='object').columns:
      for row in df.index.tolist():
          try:
              df.loc[row, column] = df.loc[row, column].lower().replace('ã', 'a').replace(
                  'º', '').replace('ó', 'o').replace('é', 'e').replace(
                  'ç', 'c').replace(' ', '_').replace('.', '').replace(
                  '-', ' ').replace(':', '_').replace('á', 'a').replace('ú', 'u').lower()
          except:
              pass

  # Formatacao da coluna ESCALA
  for row in df.index.tolist():
      if df.loc[row, 'ESCALA'][0] not in (str(0), str(1), str(2)):
          df.loc[row, 'ESCALA'] = str('0') + df.loc[row, 'ESCALA']

  # Formatacao da coluna EXP_IDENT
  for row in df.index.tolist():
      df.loc[row, 'EXP_IDENT'] = df.loc[row, 'EXP_IDENT'].replace('/','_')

  print(f'\n Feito!')
  df.to_csv('dados_normalizados.csv', index=False)

  end = time.time()
  print(f'Time elapsed:{round(end-start,3)} s')






if __name__ == '__main__':
  normaliza_dados(df_name)
